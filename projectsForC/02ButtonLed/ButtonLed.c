//
// Created by zsombornadai on 2020. 06. 19..
//

#include <wiringPi.h>
#include <stdio.h>

#define ledPin 0
#define buttonPin 1

int main(void){
    printf("Program starting....\n");

    wiringPiSetup();

    pinMode(ledPin, OUTPUT);
    pinMode(ledPin, INPUT);
    pullUpDnControl(buttonPin, PUD_UP);

    while(1){
        if(digitalRead(buttonPin) == LOW){
            digitalWrite(ledPin, HIGH);
            printf("led on\n");

        }
        else{
            digitalWrite(ledPin,LOW);
            printf("led off\n");
        }
    }
}
T