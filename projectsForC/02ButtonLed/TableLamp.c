//
// Created by zsombornadai on 2020. 07. 12..
//

#include <wiringPi.h>
#include  <stdio.h>

#define ledPin 0
#define buttonPin 1
int ledState = LOW;
int buttonState = HIGH;
int lastButtonState = HIGH;
long lastChangeTime;
long captureTime = 50;
int reading;

int main(void){
    printf("program is startrting...\n");

    wiringPiSetup();

    pinMode(ledPin, OUTPUT);
    pinMode(buttonPin, INPUT);
    pullUpDnControl(buttonPin, PUD_UP);

    while(1){
        reading = digitalRead(buttonPin);
        if(reading != lastButtonState){
            lastChangeTime = millis();
        }
        if(millis() - lastChangeTime > captureTime){
            if(reading != buttonState){
                buttonState = reading;
                if(buttonState == LOW){
                    printf("Button is pressed!\n");
                    ledState = !ledState;
                    if(ledState){
                        printf("turn on LED\n");
                    }
                    else {
                        printf("turn off LED\n");
                    }
                }
                else{
                    printf("Button is released!\n");
                }
            }
        }
        digitalWrite(ledPin,ledState);
        lastButtonState = reading;
    }
    return 0;
}