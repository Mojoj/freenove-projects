#include <wiringPi.h>
#include <stdio.h>
#define ledPi 0
int main(void){
    if(wiringPiSetup() == -1){
        printf("setup wiringPi failed !");
        return 1;
    }
    printf("wiringPi initialize successfully, GPIO %d(wiringPi pin)\n",ledPin);
    pinMode(ledPin, OUTPUT);
    while(1){
        digitalWrite(ledPin, HIGH);
        //led on
        printf("led on...\n");
        delay(1000);
        digitalWrite(ledPin, LOW);
        //led off
        printf("...led off\n");
        delay(1000);
    }
    return 0;
}
