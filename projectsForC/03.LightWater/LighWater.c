//
// Created by zsombornadai on 2020. 07. 12..
//

#include <wiringPi.h>
#include  <stdio.h>

#define ledCounts 10
int pins[ledCounts] = {1,2,3,4,5,6,7,8,9,10};

void main(void){
    int i;
    printf("program starting....\n");
    wiringPiSetup();
    for(i=0;iledCounts;i++){
        pinMode(pins[i], OUTPUT);
    }
    while(1){
        for(i=0;i<ledCounts;i++){
            digitalWrite(pins[i], LOW);
            delay(100);
            digitalWrite(pins[i], HIGH);
        }
        for(i=ledCounts-1;i>-1;i--){
            digitalWrite(pins[i], LOW);
            delay(100);
            digitalWrite(pins[i], HIGH);
        }
    }

}